#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL Ugly
#

export TMPDIR=${TMPDIR:-/tmp}

python -c "
import gstlal.aggregator
import gstlal.events
import gstlal.metric
import gstlal.tree
"

gst-inspect-1.0 gstlalugly

gstlal_condor_top --help
#gstlal_dag_run_time --help  <-- https://git.ligo.org/lscsoft/gstlal/-/issues/57
gstlal_glitch_population --help
gstlal_harmonic_mean_psd --help
gstlal_kafka_dag --help
gstlal_ligolw_add_without_reassign --help
gstlal_median_of_psds --help
gstlal_reduce_dag --help
gstlal_segments_operations --help
gstlal_segments_trim --help
