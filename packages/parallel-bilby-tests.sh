#!/bin/bash
#
# IGWN Conda Distribution tests for parallel_bilby
#

PKG_NAME="parallel-bilby"
VERSION=$(
conda list ${PKG_NAME} --json | \
python -c "import json, sys; print(json.load(sys.stdin)[0]['version'])"
)

# run everything in a temporary directory
TMPDIR=$(mktemp -d -t ${PKG_NAME}-XXXXXXXXXX)
trap "cd ~ && rm -rf ${TMPDIR}" EXIT
pushd ${TMPDIR}

# download the GitLab tarball which includes the test files
URL="https://git.ligo.org/lscsoft/parallel_bilby/-/archive/${VERSION}/parallel_bilby-${VERSION}.tar.gz"
curl -Ls ${URL} | \
tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") \
	"*/tests" \
	"*/examples" \
|| {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
python -m pytest -v -ra tests/
