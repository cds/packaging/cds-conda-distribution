#!/bin/bash
#
# IGWN Conda Distribution tests for Cm
#

# pkg-config
pkg-config --print-errors --libs cm

# start a nameserver
export CMNAMESERVERDEBUG=1
CmNameServer &
sleep 1

# kill the nameserver when we're done
CMNAMESERVER_PID="$!"
trap "kill -9 ${CMNAMESERVER_PID}" EXIT

# check cm
cm version
cm names
cm stop_server <<< "y" && trap - EXIT
