#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL Inspiral
#

export GSTLAL_FIR_WHITEN=0
export TMPDIR=${TMPDIR:-/tmp}

python -c "
import gstlal.chirptime
import gstlal.far
import gstlal.snglinspiraltable
"

gst-inspect-1.0 gstlalinspiral

gstlal_inspiral --help
gstlal_inspiral_pipe --help
gstlal_ll_inspiral_pipe --help
gstlal_svd_bank --help
