#!/bin/bash

set -ex

# create some fake data
framecpp_sample --duration 4

# validate LALFrame executables
lalfr-cksum Z-ilwd_test_frame-600000000-4.gwf
lalfr-cut Z0:RAMPED_REAL_4_1 Z-ilwd_test_frame-600000000-4.gwf > /dev/null
lalfr-dump Z-ilwd_test_frame-600000000-4.gwf
#lalfr-paste Z-ilwd_test_frame-600000000-4.gwf > /dev/null  <-- https://git.ligo.org/lscsoft/lalsuite/-/issues/415
#lalfr-split Z-ilwd_test_frame-600000000-4.gwf  <-- https://git.ligo.org/lscsoft/lalsuite/-/issues/415
lalfr-stat Z-ilwd_test_frame-600000000-4.gwf
lalfr-stream --channel Z0:RAMPED_REAL_4_1 --start-time 600000000 --duration 0.01 --frame-glob Z-ilwd_test_frame-*-4.gwf
lalfr-vis --channel Z0:RAMPED_REAL_4_1 --start-time 600000000 --duration 0.01 --frame-glob Z-ilwd_test_frame-*-4.gwf
