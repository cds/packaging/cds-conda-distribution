#!/bin/sh

set -ex

# use the open-science gwdatafind server
export GWDATAFIND_SERVER="datafind.gw-openscience.org:80"

# check that we can ping the server
python -m gwdatafind --ping || {
	echo "gwdatafind ping test failed, skipping..." 2>&1;
	exit 77;
}

# check that we can query for types
python -m gwdatafind --show-types

# check that the latest file for an old dataset is what we think it should be
test \
	$(python -m gwdatafind -o H -t H1_GWOSC_O2_4KHZ_R1 --latest | xargs basename) == \
	"H-H1_GWOSC_O2_4KHZ_R1-1187733504-4096.gwf"
