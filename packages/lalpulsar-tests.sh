#!/bin/bash
#
# IGWN Conda Distribution tests for LALPulsar
#

# liblalpulsar
pkg-config --print-errors --libs lalpulsar

# python-lalpulsar
python -c "
import lalpulsar.NstarTools
import lalpulsar.PulsarParametersWrapper
import lalpulsar.simulateCW
"

# lalpulsar
lalpulsar_version
