#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""IGWN Conda Distribution tests for python-gds
"""

import gds.pygdsdata
import gds.pygdsio
import gds.pygdssigp


def test_time():
    """Test that ``Time`` does what we think it should
    """
    t = gds.pygdsdata.Time(12345, 12345)
    assert t.totalS() == 12345.000012345


if __name__ == "__main__":
    import sys
    import pytest
    sys.exit(pytest.main(args=[__file__] + sys.argv[1:]))
