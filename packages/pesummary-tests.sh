#!/bin/bash
#
# IGWN Conda Distribution tests for PESummary
#

# run everything in a temporary directory
TMPDIR=$(mktemp -d -t lscsoft-glue-XXXXXXXXXX)
trap "cd ~ && rm -rf ${TMPDIR}" EXIT
cd ${TMPDIR}

# set XDG_CACHE_DIR to redirect pesummary logs
export XDG_CACHE_DIR="$(pwd)/.cache"

# run the pytest suite
python -m pytest \
	-ra \
	--verbose \
	--pyargs pesummary.tests.read_test \
;
