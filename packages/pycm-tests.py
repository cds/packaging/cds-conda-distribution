#!/usr/bin/env python
#
# IGWN Conda Distribution tests for PyCm
#

import PyCm as cm


def test_install_handler():
    @cm.CmMessageHandler
    def Handler(message, handler, _):
        return cm.CmMessageOk

    cm.CmMessageInstallDefaultHandler(Handler)


if __name__ == "__main__":
    import sys
    import pytest
    sys.exit(pytest.main(args=[__file__] + sys.argv[1:]))
