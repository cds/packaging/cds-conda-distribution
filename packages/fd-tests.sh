# temporary work directory (and delete it when we're done)
WORKDIR=$(mktemp -d)
trap "rm -rf ${WORKDIR}" EXIT

cat <<EOF > ${WORKDIR}/fdioserver.cfg
#Ensure that cm isn't used
CFG_CMDOMAIN None

# Current logfile path <path>/<cmName>
CFG_PWD ${WORKDIR}

#Open up a test frame
FDIN_NEW_FRAME Test 1 1000000000 1 1

#Add Test channels to frames
FDIN_ADD_TEST_CHANNEL V1:test0 "h" 50 32  10.0 0.5 5 32 1.
EOF

#Run a test FdIOServer
FdIOServer ${WORKDIR}/fdioserver.cfg
