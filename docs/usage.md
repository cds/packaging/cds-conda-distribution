# Using the IGWN Conda Distribution

This page describes how to use the the IGWN Conda Distribution, either
via CVMFS, or with a standalone conda configuration.

!!! note "OS support"
    The IGWN Conda Distribution is fully tested and built on 64-bit Linux only,
    support for macOS, Windows, or other platforms is on a best-effort
    basis only.

!!! info "Refer to the conda User Guide"
    Please refer to the conda User Guide for more information on how
    to configure conda and manage environments:

    <https://docs.conda.io/projects/conda/en/latest/user-guide/>

## Local usage {: #local }

This section is designed for users of conda on a local machine, i.e. a laptop
or desktop workstation.
Users of the IGWN shared computing centres should refer to the
[Grid usage](#grid) section below.

This guide presents a tiny subset of the conda capabilities, for more
information see the links at the bottom of the page.

### 1. Install conda {: #install }

The first thing to do is to install conda itself.
We recommend installing conda from
[Mambaforge](https://github.com/conda-forge/miniforge#miniforge):

=== "Unix (Linux/macOS)"
    1. Download the installer on the command line:

        ```bash
        curl -L -O https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh
        ```

    2. Install it:

        ```bash
        bash Mambaforge-$(uname)-$(uname -m).sh
        ```

        Note: the installer script will present a few questions to answer.

=== "Windows"
    1. Click [here](https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge3-Windows-x86_64.exe) to download the installer.

    2. Double-click the installer file in your file browser.

### 2. Check the conda configuration {: #check }

When installed correctly, you should be able to open a new terminal
window and verify that conda works correctly:

```bash
conda info
```

This should produce output that looks something like this:

```console
> conda info

     active environment : None
       user config file : C:\Users\mariecurie\.condarc
 populated config files : C:\Users\mariecurie\.condarc
          conda version : 4.10.3
    conda-build version : 3.21.4
         python version : 3.7.6.final.0
       virtual packages : __win=0=0
                          __archspec=1=x86_64
       base environment : C:\Users\mariecurie\Miniconda3  (writable)
      conda av data dir : C:\Users\mariecurie\Miniconda3\etc\conda
  conda av metadata url : None
           channel URLs : https://conda.anaconda.org/conda-forge/win-64
                          https://conda.anaconda.org/conda-forge/noarch
                          https://repo.anaconda.com/pkgs/main/win-64
                          https://repo.anaconda.com/pkgs/main/noarch
                          https://repo.anaconda.com/pkgs/r/win-64
                          https://repo.anaconda.com/pkgs/r/noarch
                          https://repo.anaconda.com/pkgs/msys2/win-64
                          https://repo.anaconda.com/pkgs/msys2/noarch
          package cache : C:\Users\mariecurie\Miniconda3\pkgs
                          C:\Users\mariecurie\.conda\pkgs
                          C:\Users\mariecurie\AppData\Local\conda\conda\pkgs
       envs directories : C:\Users\mariecurie\Miniconda3\envs
                          C:\Users\mariecurie\.conda\envs
                          C:\Users\mariecurie\AppData\Local\conda\conda\envs
               platform : win-64
             user-agent : conda/4.10.3 requests/2.26.0 CPython/3.7.6 Windows/10 Windows/10.0.19041
          administrator : False
             netrc file : None
           offline mode : False
```

The output here will depend upon your choice of install location, and the
operating system you are running.

#### Ensure that the conda-forge channel is configured {: #conda-forge }

IGWN software is uploaded to the conda-forge package channel only.
If you don't see references to `conda-forge` in the output of `conda info`
above, you will have to manually configure the conda-forge
channel as follows:

```shell
conda config --add channels conda-forge
conda config --set channel_priority strict
```

### 3. Create a new environment {: #create }

Each installation of conda comes with a default environment named `base`;
this is the environment that contains `conda` itself.
It is __highly recommended__ to not install scientific software into the
`base` environment, instead you should create a new environment.
This will help make sure that your `conda` installer doesn't become inoperable,
and can be upgraded independently of the scientific software stack you are using.

To create a new environment called `igwn`, run the following in a
terminal window:

```shell
conda create --name igwn
```

### 4. Activate the new environment {: #activate }

To use the environment, it must be 'activated', type the following:

```bash
conda activate igwn
```

You should then find that any scripts installed into the new environment
are available for use, at the moment we don't have anything.

If you wish, when you finish working in an environment, you can `deactivate`
it via

```bash
conda deactivate igwn
```

This should return your shell session back to the same state it was in
before you activated the environment.

### 5. Install some packages {: #install }

To install packages into the activate environment, just run
`conda install` and pass it the names of all of the packages you want.
For example, to install Python 3.9 and GWpy:

```shell
conda install "python=3.9" gwpy
```

Conda will then work out what exact versions of Python, GWpy and any
other packages (dependencies) to install, and might ask you to proceed:

```console
Proceed ([y]/n)?
```

Type `y` and press Enter to proceed.

You should immediately be able to start using the new software you have
installed.

To install software into a different environment that isn't the active one,
you can pass `--name <env>` to the `conda install` command:

```shell
conda install --name igwn gwpy
```

!!! tip "Quote package names when specifying versions"
    To minimise the chance of your shell getting confused, it's a good idea
    to put quotation marks around package names when calling `conda install`
    or similar commands.
    This is especially important when adding version specifiers to the
    package name, e.g. `conda install gwpy>=2.1.0` will probably do something
    you don't want it to, but `conda install "gwpy>=2.1.0"` will do what it's
    supposed to.

!!! example "Use `conda list` to list packages in an environment"
    The `conda list` command can be used to print the list of packages
    in an environment.

### 6. Updating packages {: #update }

#### Updating specific packages {: #update-one }

To update packages in an environment, simply run the following in a terminal
window:

```shell
conda update <package> <package>
```

e.g.

```shell
conda update gwpy
```

Conda will then work out what to do to update the requested packages and any
upstream packages that need to be updated at the same time.

!!! note "Conda might not update absolutely everything"
    In order to solve the environment to update most packages, conda may
    end up not updating some packages to the absolutely latest version.
    This will happen if the latest version of PackageA conflicts with
    the latest version of PackageB.

    If you absolutely require that PackageA is updated, you should manually
    update that one using `conda update packagea`.

#### Updating all packages {: #update-all }

To update all packages in an environment, run

```shell
conda update --all
```

## Grid usage {: #grid }

This section is designed for users of conda on an IGWN shared computing centre.
Users of an IGWN shared computing centre will be able to use the
centrally-managed.

### 1. Configure conda {: #configure }

On all IGWN shared computing centres, `conda` should be pre-built and
pre-configured without any manual steps required from the user.

### 2. Activate a pre-built environment {: #activate }

The IGWN Conda Distribution on OASIS provides a number of pre-built
environments for Linux users, see [_Environments_](environments/index.md)
for full details of what environments are available, or run

```shell
conda env list
```

for a complete list.
Once you have chosen, simply run

```shell
conda activate <env>
```

to activate the relevant environment.

!!! example "Example: activating the `igwn-py39` environment from OASIS"

    ```bash
    conda activate igwn-py39
    ```

## More reading {: #links }

For more information on conda and its usage, please refer to one of the
following sources:

- <https://conda.io/projects/conda/en/latest/user-guide/getting-started.html>
- <https://conda.io/projects/conda/en/latest/user-guide/cheatsheet.html>
- <https://jakevdp.github.io/blog/2016/08/25/conda-myths-and-misconceptions/>

For more advanced usage tips related to use of conda for IGWN research,
please refer to [_Advanced usage_](tips.md).

