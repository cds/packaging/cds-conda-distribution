#!/bin/bash
#
# IGWN Conda Distribution tests for gds-gui-crtools
#

set -ex

# test loading some libraries with root
for _rlib in dttview gdsplot ligogui; do
    root \
        -b \
        -l \
        -x \
        -q \
        -e "
gInterpreter->AddIncludePath(\"${CONDA_PREFIX}/include/gds\");
gSystem->Load(\"libR${_rlib}\");
"
done
