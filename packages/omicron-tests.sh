#!/bin/bash
#
# IGWN Conda Distribution tests for Omicron
#

# run test scripts
for omitest in ${CONDA_PREFIX}/sbin/omicron-test-*; do
    ${omitest};
done
