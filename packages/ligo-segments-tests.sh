#!/bin/bash
#
# IGWN Conda Distribution tests for ligo-segments
#

# run everything in a temporary directory
TMPDIR=$(mktemp -d -t ligo-segments-XXXXXXXXXX)
trap "cd ~ && rm -rf ${TMPDIR}" EXIT
pushd ${TMPDIR}

# download the original tarball which includes the test/
VERSION=$(python -c "import ligo.segments; print(ligo.segments.__version__)")
URL="https://pypi.io/packages/source/l/ligo-segments/ligo-segments-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/test" || {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
make -C test check VERBOSE=1 V=1
